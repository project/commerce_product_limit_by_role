The Commerce product limit by role module allows the user to set the control on buying "Product Variation" by restrict the role.

FEATURES:
------------

User can restricted single or multiple role at a time.

REQUIREMENTS
------------

This module requires the following modules:

* Commerce (https://www.drupal.org/project/commerce)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

CONFIGURATION
-------------

To use:
1. Navigate to Administration > Extend and enable the module and it's dependencies.
2. In the variation for create and update "Limit By Roles" field will be appeared.